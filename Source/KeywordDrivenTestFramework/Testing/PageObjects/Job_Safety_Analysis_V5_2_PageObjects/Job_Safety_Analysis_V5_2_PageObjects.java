/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Job_Safety_Analysis_V5_2_PageObjects;

/**
 *
 * @author smabe
 */
public class Job_Safety_Analysis_V5_2_PageObjects {

    public static String Occupational_Health() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }
    public static String IM_processFlow() {
        return "//div[@id='btnProcessFlow_form_B6196CB4-4610-463D-9D54-7B18E614025F']";
    }
    public static String InherentImpact_Dropdown() {
        return "//div[@id='control_456095C7-CFB8-498D-97C2-298826620BB4']";
    }
    public static String Impact_Dropdown_Option(String data) {
        return "(//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]//..//i)[1]";
    }
    public static String Impact_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String InherentLikelihood_Dropdown() {
        return "//div[@id='control_DF43CC47-1C7F-46EA-A0D3-2AE936412A29']";
    }
    public static String JSA_Action_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_4B0A452E-D2E7-463B-8888-8498C3CCE782']";
    }
    public static String JSA_Actions_CloseButton() {
        return "(//div[@id='form_4B0A452E-D2E7-463B-8888-8498C3CCE782']//i[@class='close icon cross'])[1]";
    }
    public static String Likelihood_Option(String data) {
        return "//div[contains(@class, 'transition visible')]//a[contains(text(),'" + data + "')]";
    }
    public static String JSA_Assessment_CloseButton() {
        return "(//div[@id='form_4629C9F7-B165-442D-B6CB-4E18D95B41EE']//i[@class='close icon cross'])[1]";
    }
    public static String JSA_Assessment_ProcessFlow() {
        return "//div[@id='btnProcessFlow_form_4629C9F7-B165-442D-B6CB-4E18D95B41EE']";
    }
    public static String JSA_Actions_Record(String data) {
        return "((//div[@id='control_6BA2F65C-11A2-451D-9E0D-C9938FAD8855']//div//table)[3]//div[text()='" + data + "'])[1]";
    }
    public static String JSA_Assessment_Record() {
        return "((//div[@id='control_6BA2F65C-11A2-451D-9E0D-C9938FAD8855']//div//table)[3]//tr)[1]";
    }
    public static String RelateIncidentManagement_Record(String data) {
        return "((//div[@id='control_E176C9A2-FCB0-4242-B487-088D258B5361']//div//table)[3]//div[text()='" + data + "'])[1]";
    }
    public static String RelateIncident_Record() {
        return "((//div[@id='control_85F17681-0624-4253-80D8-2EAE84764E3F']//div//table)[3]//tr)[1]";
    }
    
    
    public static String Project_Dropdown() {
        return "//div[@id='control_2B3BB054-E08D-47B3-BE0A-3DF0AF3CF88B']";
    }
    public static String Project_Option(String data) {
        return "//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')]";
    }
    public static String RelatedIncident_Panel() {
        return "//span[text()='Related Incidents']";
    }
    public static String ResidualImpact_Dropdown() {
        return "//div[@id='control_20912F62-0ACC-4844-9FAE-2B271622E5F8']";
    }
    public static String ResidualLikelihood_Dropdown() {
        return "//div[@id='control_CCF92B2E-2338-4B7A-8AD1-C501CE71031F']";
    }
    public static String Task_Dropdown_Option(String data) {
        return "(//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')]//..//i)[1]";
    }
    public static String Task_Option(String data) {
        return "//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')]";
    }
    public static String Username1() {
        return "//input[@id='txtUsername']";
    }
    public static String Social_Sustainability() {
        return "//label[contains(text(),'Social Sustainability')]";
    }
    public static String Environment_Health_Safety() {
        return "//label[contains(text(),'Environment, Health & Safety')]";
    }
    public static String Risk_Management_Label() {
        return "(//label[contains(text(),'Risk Management')])[2]";
    }
    public static String Job_Safety_Analysis_Label() {
        return "(//label[contains(text(),'Job Safety Analysis')])";
    }
    public static String Job_Safety_Analysis_Add_Button() {
        return "//div[@id='btnActAddNew']//div[text()='Add']";
    }
    public static String Process_Flow_Button() {
        return "//div[@id='btnProcessFlow_form_EBF4B94D-9E08-49BD-9899-CD64DC78AE1D']";
    }
    public static String AssessmentDate() {
        return "//div[@id='control_D4FAA298-F13B-4A95-B59D-80C39035DFA6']//input";
    }
    public static String ValidityDate() {
        return "//div[@id='control_1C0A16FB-2FFE-4267-9DCC-2F55C380A9E2']//input";
    }
    public static String Create_JSA_Drop_Down() {
        return "//div[@id='control_AB1865F2-6050-4063-94BF-68522DD588DB']//ul//li";
    }
    public static String alarp_Dropdown() {
        return "//div[@id='control_3CA75061-04F6-4490-A347-002F6E8899F9']";
    }
    public static String saveToContinue_Button() {
        return "//div[@id='control_8FC784A7-42E1-4F5F-827C-50E5BCBEF866']//div[text()='Save and continue']";
    }
    public static String visible_drop_down_transition(String data) {
        return "//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')]";
    }
    public static String visible_drop_down_transition_3(String data) {
        return "(//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')])[2]";
    }
    public static String visible_drop_down_transition_2(String data) {
        return "(//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')])[3]";
    }
    public static String visible_drop_down_transition_option(String data) {
        return "(//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')]//..//i)[1]";
    }
    public static String Process_option(String data) {
        return "(//div[contains(@class,'transition visible')]//a[contains (text(),'" + data + "')])[1]";
    }
    public static String JSA_title() {
        return "//div[@id='control_82EA4A56-80CB-43DA-A0C8-15CFEFC31341']//input";
    }
    public static String JSA_scope_description() {
        return "//div[@id='control_F2F4D7B1-1E2C-42A3-9FBF-B07F61189BF4']//textarea";
    }
    public static String Project_link() {
        return "//div[@id='control_C329409D-EE14-4961-AF31-FFFDB7D4008E']";
    }
    public static String Related_stakeholder_drop_down() {
        return "//div[@id='control_CD8E38C0-5A14-42AB-AFA7-A15621F17A0F']//ul";
    }
    public static String Type_search() {
        return "(//div[contains(@class,'transition visible')]//input[@placeholder])[4]";
    }
    public static String Responsible_person_drop_down() {
        return "//div[@id='control_ECA37EEF-DDB6-4FCA-BE00-4023CBB597FB']//ul";
    }
    public static String Responsible_person_drop_down_2() {
        return "//div[@id='control_7854D003-23E6-4A2E-AF2E-357C965FA684']//ul";
    }
    public static String saveBtn() {
        return "//div[@id='btnSave_form_EBF4B94D-9E08-49BD-9899-CD64DC78AE1D']";
    }
    public static String saveBtn_2() {
        return "//div[@id='btnSave_form_4629C9F7-B165-442D-B6CB-4E18D95B41EE']";
    }
    public static String saveBtn_3() {
        return "//div[@id='btnSave_form_4B0A452E-D2E7-463B-8888-8498C3CCE782']";
    }
    public static String saveWait() {
        return "//div[@class='ui inverted dimmer active']";
    }
    public static String saveWait2() {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }
    public static String Record_Saved_popup() {
        return "//div[@id='divForms']//div[contains(text(),'Record saved')]";
    }
    public static String failed() {
        return "(//div[@id='txtAlert'])[2]";
    }
    public static String Business_unit_drop_down() {
        return "//div[@id='control_29ABF821-7ACB-4AA5-BEAA-67CAB4CC51A2']//ul";
    }
    public static String Process_Activity_drop_down() {
        return "//div[@id='control_17F0AD4B-8FFC-4076-9143-67754071F073']//ul";
    }
    public static String Relevant_procedure_drop_down() {
        return "//div[@id='control_4E30AD02-F4D3-4672-8D00-80205F69CEBB']//ul";
    }
    public static String Job_Safety_Analysis_Assessment_Tab() {
        return "//div//span[text()='Job Safety Analysis Assessment']";
    }
    public static String Job_Safety_Analysis_Assessment_Record() {
        return "(//div[@class='grid k-grid k-widget k-display-block k-reorderable']//div[@class='k-grid-content k-auto-scrollable'])[2]//tr[1]";
    }
    public static String Hazards() {
        return "//div[@id='control_335297C0-E195-44C6-9290-258E6560F819']//input";
    }
    public static String Controls() {
        return "//div[@id='control_969E9736-37F9-49A9-AC6A-7F2A290A0556']//input";
    }
    public static String Recommendations() {
        return "//div[@id='control_2E2C6E16-8E59-416A-8936-F1104A32CF02']//input";
    }
    public static String Preventative_actions() {
        return "//div[@id='control_BA9D1572-01A5-4728-8025-4EC9316104A9']//input";
    }
    public static String Job_Safety_Analysis_Actions_Tab() {
        return "//div//span[text()='Job Safety Analysis Actions']";
    }
    public static String Add_button() {
        return "//div[text()='Job Safety Analysis Actions']/..//div[@id='btnAddNew']";
    }
    public static String Action_description() {
        return "//div[@id='control_1255F613-A69C-476A-8B05-4B87E5CA009F']//textarea";

    }
    public static String Department_responsible_unit_drop_down() {
        return "//div[@id='control_34D02E21-7837-484C-844E-BCC8CC077837']//ul";
    }
    public static String Action_due_date() {
        return "//div[@id='control_A1A7A250-4916-472D-A6A5-CDA980F5DA52']//input";
    }
    public static String Job_Safety_Analysis_Assessment_Add_Button() {
        return "//div[@id='control_B80750E4-BF1E-4209-9AFC-AF59264DC661']//div[text()='Add']";
    }
    public static String Step_number() {
        return "(//div[@id='control_EE83A40C-6C9E-461F-B58B-091F8186087A']//input)[1]";
    }
    public static String Task() {
        return "//div[@id='control_B25B24A8-0382-4E74-8FE9-C8B7D58EE64C']//ul";
    }
    public static String Activity() {
        return "//div[@id='control_2A28F2D7-340C-40E5-B5D4-8137B5020E00']//input";
    }
    public static String Task_Drop_Down_Option() {
        return "(//a[text()='Environmental']/..//i)[3]";
    }
    
    public static String office_URL() {
    return "https://www.office.com";
    }
    
    public static String office_signin() {
    return ".//a[contains(text(),'Sign in')]";
    }
    
    public static String office_email_id(){
    return ".//input[@type='email']";
    }
    
    public static String email_next_btn(){
    return ".//input[@value='Next']";
    }
    
    public static String office_password(){
    return "//input[@type='password']";
    }
    
    public static String office_signin_btn(){
    return "//input[@value='Sign in']";
    }
    
    public static String office_No_btn(){
    return "//input[@value='No']";
    }
    
    public static String outlook_icon(){
    return ".//div[@id='ShellMail_link_text']";
    }
    
    public static String inbox_chevron_expand(){
    return "//i[@class='ms-Button-icon _1IolX_6rKX93IRLO1O_oCP _3o738zmfzs1fXK1kxpiX5 _1OPSsVxfk_GWTnwo-KIMYX flipForRtl icon-60']";
    }
    
    public static String system_mail_folder(){
    return "//span[text()='System Mail']";
    }
    
    public static String email_notification_Logged(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Job Safety Analysis')][contains(text(),'has been logged')])[1]";
    }
    
    public static String email_notification_Actions(){
    return "(//span[contains(text(),'NON PRODUCTION - IsoMetrix Job Safety Analysis Actions')][contains(text(),'has been logged')])[1]";
    }

    public static String ActionsRecordNumber_xpath() {
    return "(//div[@id='form_DB1AFD7D-40B5-47B3-A866-45A3E2F86848']//div[contains(text(),'- Record #')])[1]";
    }
    
    public static String linkBackToRecord_Link(){
    return ".//a[@data-auth='NotApplicable']";
    }
    
    public static String Username() {
    return "//input[@id='txtUsername']";
    }

    public static String Password() {
    return "//input[@id='txtPassword']";
    }

    public static String LoginBtn() {
    return "//div[@id='btnLoginSubmit']";
    }
    
    
}
