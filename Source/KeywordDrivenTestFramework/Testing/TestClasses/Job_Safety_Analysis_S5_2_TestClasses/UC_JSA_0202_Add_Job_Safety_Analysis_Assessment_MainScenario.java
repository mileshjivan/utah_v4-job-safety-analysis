/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Job_Safety_Analysis_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_V5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "UCJSA0202-Add job safety analysis assessment v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class UC_JSA_0202_Add_Job_Safety_Analysis_Assessment_MainScenario extends BaseClass {
    String error = "";

    public UC_JSA_0202_Add_Job_Safety_Analysis_Assessment_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Job_Safety_Analysis()) {
            return narrator.testFailed("Job Safety Analysis Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Job Safety Analysis Record");

    }

    public boolean Capture_Job_Safety_Analysis() {
        //Job Safety Analysis Assessment
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Tab())) {
            error = "Failed to wait for Job Safety Analysis Assessment tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Tab())) {
            error = "Failed to click Job Safety Analysis Assessment tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Assessment tab");

        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Add_Button())) {
            error = "Failed to wait for Job Safety Analysis Assessment add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Add_Button())) {
            error = "Failed to click Job Safety Analysis Assessment add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Assessment add button");

        pause(5000);
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_ProcessFlow())) {
            error = "Failed to wait for Job Safety Analysis Assessment Process Flow.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_ProcessFlow())) {
            error = "Failed to click the Job Safety Analysis Assessment Process Flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Assessment Process Flow");

        //Step number
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Step_number())) {
            error = "Failed to wait for Step number";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Step_number(), getData("Step number"))) {
            error = "Failed to enter Step number :" + getData("Step number");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Step number :" + getData("Step number"));

        //Task
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Task())) {
            error = "Failed to wait for Task";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Task())) {
            error = "Failed to click for Task";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Task_Dropdown_Option(getData("Task 1")))) {
            error = "Failed to wait for Task option: " + getData("Task 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Task_Dropdown_Option(getData("Task 1")))) {
            error = "Failed to click on Task option: " + getData("Task 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Task_Option(getData("Task 2")))) {
            error = "Failed to click on Task option: " + getData("Task 2");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully slected Task: " + getData("Task 1") + " -> " + getData("Task 2"));

        //Activity
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Activity(), getData("Activity"))) {
            error = "Failed to enter Activity :" + getData("Activity");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Activity :" + getData("Activity"));

        //Hazards
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Hazards(), getData("Hazards"))) {
            error = "Failed to enter Hazards :" + getData("Hazards");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Hazards :" + getData("Hazards"));

        //Controls
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Controls(), getData("Controls"))) {
            error = "Failed to enter Hazards :" + getData("Controls");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Controls :" + getData("Controls"));

        //-------------------------------------
        //Inherent impact
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.InherentImpact_Dropdown())) {
            error = "Failed to wait for Inherent impact dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.InherentImpact_Dropdown())) {
            error = "Failed to click the Inherent impact dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Impact_Dropdown_Option(getData("Inherent impact 1")))) {
            error = "Failed to wait for Inherent impact option: " + getData("Inherent impact 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Impact_Dropdown_Option(getData("Inherent impact 1")))) {
            error = "Failed to click the Inherent impact option: " + getData("Inherent impact 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Impact_Dropdown_Option(getData("Inherent impact 2")))) {
            error = "Failed to click the Inherent impact option: " + getData("Inherent impact 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Impact_Option(getData("Inherent impact 3")))) {
            error = "Failed to click the Inherent impact option: " + getData("Inherent impact 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Inherent impact option: " + getData("Inherent impact 1") + " -> " + getData("Inherent impact 2") + " -> " + getData("Inherent impact 3"));

        //Inherent likelihood
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.InherentLikelihood_Dropdown())) {
            error = "Failed to wait for Inherent likelihood dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.InherentLikelihood_Dropdown())) {
            error = "Failed to click the Inherent likelihood dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Likelihood_Option(getData("Inherent likelihood")))) {
            error = "Failed to wait for Inherent likelihood option: " + getData("Inherent likelihood");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Likelihood_Option(getData("Inherent likelihood")))) {
            error = "Failed to click the Inherent likelihood option: " + getData("Inherent likelihood");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Inherent likelihood: " + getData("Inherent likelihood"));

        //-------------------------------------
        //Residual impact
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.ResidualImpact_Dropdown())) {
            error = "Failed to wait for Residual impact dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.ResidualImpact_Dropdown())) {
            error = "Failed to click the Residual impact dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Impact_Dropdown_Option(getData("Residual impact 1")))) {
            error = "Failed to wait for Residual impact option: " + getData("Residual impact 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Impact_Dropdown_Option(getData("Residual impact 1")))) {
            error = "Failed to click the Residual impact option: " + getData("Residual impact 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Impact_Dropdown_Option(getData("Residual impact 2")))) {
            error = "Failed to click the Residual impact option: " + getData("Residual impact 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Impact_Option(getData("Residual impact 3")))) {
            error = "Failed to click the Residual impact option: " + getData("Residual impact 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Residual impact option: " + getData("Residual impact 1") + " -> " + getData("Residual impact 2") + " -> " + getData("Residual impact 3"));

        //Inherent likelihood
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.ResidualLikelihood_Dropdown())) {
            error = "Failed to wait for Residual likelihood dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.ResidualLikelihood_Dropdown())) {
            error = "Failed to click the Residual likelihood dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Likelihood_Option(getData("Residual likelihood")))) {
            error = "Failed to wait for Residual likelihood option: " + getData("Residual likelihood");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Likelihood_Option(getData("Residual likelihood")))) {
            error = "Failed to click the Residual likelihood option: " + getData("Residual likelihood");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Residual likelihood: " + getData("Residual likelihood"));

        //ALARP
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.alarp_Dropdown())) {
            error = "Failed to wait for ALARP dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.alarp_Dropdown())) {
            error = "Failed to click the ALARP dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Likelihood_Option(getData("ALARP")))) {
            error = "Failed to wait for ALARP Option: " + getData("ALARP");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Likelihood_Option(getData("ALARP")))) {
            error = "Failed to click the ALARP Option: " + getData("ALARP");
            return false;
        }
        narrator.stepPassedWithScreenShot("ALARP Option: " + getData("ALARP"));

        //Recommendations
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Recommendations())) {
            error = "Failed to wait for Recommendations";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Recommendations(), getData("Recommendations"))) {
            error = "Failed to enter Recommendations :" + getData("Recommendations");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Recommendations :" + getData("Recommendations"));

        //Corrective / Preventative actions
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Preventative_actions())) {
            error = "Failed to wait for Corrective / Preventative actions input fields";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Preventative_actions(), getData("Corrective / Preventative actions"))) {
            error = "Failed to enter Corrective / Preventative actions :" + getData("Corrective / Preventative actions");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Corrective / Preventative actions :" + getData("Corrective / Preventative actions"));

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.saveBtn_2())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.saveBtn_2())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Record_Saved_popup())) {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Record_Saved_popup())) {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.failed())) {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved")) {
                error = "Failed to save record.";
                return false;
            }
        }

        pause(5000);
        
        //Close Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_CloseButton())) {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_CloseButton())) {
                error = "Failed to wait for the JSA Assessment close button.";
                return false;
            }
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_CloseButton())) {
            error = "Failed to click the JSA Assessment close button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked JSA Assessment.");
        pause(2000);
        //Updated record
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_Record())){
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_Record())){
                error = "Failed to wait for the Job Safety Analysis Assessment Record.";
                return false;
            }
        }

        return true;
    }

}
