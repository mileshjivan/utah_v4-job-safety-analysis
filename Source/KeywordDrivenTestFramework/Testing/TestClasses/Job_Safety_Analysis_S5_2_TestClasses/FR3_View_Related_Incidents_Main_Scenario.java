/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Job_Safety_Analysis_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_V5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR3-View Related Incidents v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_View_Related_Incidents_Main_Scenario extends BaseClass {
    String error = "";

    public FR3_View_Related_Incidents_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!View_Related_Incidents()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully View Related Incidents Record");

    }

    public boolean View_Related_Incidents() {
        //Updated record
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_Record())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_Record())){
                error = "Failed to wait for the Job Safety Analysis Assessment Record.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_Record())){
            error = "Failed to click the Job Safety Analysis Assessment Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Job Safety Analysis Assessment Record.");
        
        pause(15000);
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_ProcessFlow())){
            error = "Failed to wait for Job Safety Analysis Assessment Process Flow.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_ProcessFlow())){
            error = "Failed to click the Job Safety Analysis Assessment Process Flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Assessment Process Flow");
        
        //Related Incidents
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.RelatedIncident_Panel())){
            error = "Failed to wait for the Related Incidents Panel.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.RelatedIncident_Panel())){
            error = "Failed to click for the Related Incidents Panel.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Related Incidents Panel..");
 
        //Related Incident Management Records
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.RelateIncident_Record())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.RelateIncident_Record())){
                error = "Failed to wait for the Related Incident Management Record.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.RelateIncident_Record())){
            error = "Failed to click the Related Incident Management Record.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully Job Safety Analysis Assessment Record.");
        
        pause(15000);
        
        //Incident Management Process Flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.IM_processFlow())){
            error = "Failed to wait for the Process flow.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.IM_processFlow())){
            error = "Failed to click the Process flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Incident Management Process Flow..");
        
        return true;
    }

}
