/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Job_Safety_Analysis_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_V5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR4-Capture JSA Actions v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR4_Capture_JSA_Actions_Main_Scenario extends BaseClass {
    String error = "";

    public FR4_Capture_JSA_Actions_Main_Scenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Caputure_Actions_Job_Safety_Analysis()) {
            return narrator.testFailed("Job Safety Analysis Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Job Safety Analysis Actions Record");
    }

    public boolean Caputure_Actions_Job_Safety_Analysis(){
        //Job Safety Analysis Acton tab.
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Actions_Tab())) {
            error = "Failed to wait for Job Safety Analysis Acton tab.";
            return false;
        }
        if (!SeleniumDriverInstance.scrollToElement(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Actions_Tab())) {
            error = "Failed to scrollto the Job Safety Analysis Acton tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Actions_Tab())) {
            error = "Failed to click Job Safety Analysis Acton tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Acton tab");

        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Add_button())) {
            error = "Failed to wait for Add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Add_button())) {
            error = "Failed to click Add button";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Add button");

        pause(15000);
        
        //Process flow
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Action_ProcessFlow())){
            error = "Failed to wait for Job Safety Analysis Actions Process Flow.";
            return false;
        }
        pause(2000);
        if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Action_ProcessFlow())){
            error = "Failed to click the Job Safety Analysis Actions Process Flow.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Action Process Flow");
        
        //Action description
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Action_description())) {
            error = "Failed to wait for Action description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Action_description(), getData("Action description"))) {
            error = "Failed to wait for Action description";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Action description :" + getData("Action description"));

        //Department responsible
        //Business unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Department_responsible_unit_drop_down())) {
            error = "Failed to wait for Department responsible drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Department_responsible_unit_drop_down())) {
            error = "Failed to click on Department responsible drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Business unit 1")))) {
            error = "Failed to wait for Department responsible drop down option :" + getData("Business unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition_option(getData("Business unit 1")))) {
            error = "Failed to click on Department responsible drop down option :" + getData("Business unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Business unit 2")))) {
            error = "Failed to wait for Department responsible drop down option :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition_option(getData("Business unit 2")))) {
            error = "Failed to click on BDepartment responsible drop down option :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Business unit 3")))) {
            error = "Failed to wait for Department responsible drop down option :" + getData("Business unit 3");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Business unit 3")))) {
            error = "Failed to click on Department responsibledrop down option :" + getData("Business unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Department responsible drop down option :" + getData("Business unit 3"));

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Responsible_person_drop_down_2())) {
            error = "Failed to wait for Responsible person drop down ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Responsible_person_drop_down_2())) {
            error = "Failed to wait for Responsible person drop down ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Responsible person drop down ");

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Responsible person")))) {
            error = "Failed to wait for Responsible person option :" + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Responsible person")))) {
            error = "Failed to click on Responsible person option :" + getData("Responsible person");
            return false;
        }

        //Action due date  
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Action_due_date())) {
            error = "Failed to wait for Action due date Start date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Action_due_date(), startDate)) {
            error = "Failed to enter Action due date '" + startDate + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Action due date: '" + startDate + "'.");

        //Save
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.saveBtn_3())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.saveBtn_3())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

         //Save mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        pause(2000);

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Job_Safety_Analysis_V5_2_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");
        
        pause(5000);
        
        //Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Actions_CloseButton())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Actions_CloseButton())){
                error = "Failed to wait for the JSA Actions close button.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Actions_CloseButton())){
            error = "Failed to click the JSA Actions close button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked JSA Actions.");
        pause(2000);
        //Updated record
        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Actions_Record(getData("Action description")))){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Actions_Record(getData("Action description")))){
                error = "Failed to wait for the Job Safety Analysis Action Record.";
                return false;
            }
        }

         //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Process_Flow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Process_Flow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");
        
        return true;
    }

}
