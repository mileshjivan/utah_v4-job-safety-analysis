/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Job_Safety_Analysis_S5_2_TestClasses;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_V5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR1-Record A Job Safety Analysis v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_Record_A_Job_Safety_Analysis_MainScenario extends BaseClass {
    String error = "";

    public FR1_Record_A_Job_Safety_Analysis_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Navigate_To_Environmental_Health_Safety()) {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!Job_Safety_Analysis()) {
            return narrator.testFailed("Job Safety Analysis Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Capture Job Safety Analysis Record");
    }

    public boolean Navigate_To_Environmental_Health_Safety() {
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Environment_Health_Safety())) {
            error = "Failed to wait for Environment, Health & Safety tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Environment_Health_Safety())) {
            error = "Failed to click Environment, Health & Safety tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Environment, Health & Safety tab");

        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Risk_Management_Label())) {
            error = "Failed to wait for Risk Management tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Risk_Management_Label())) {
            error = "Failed to click Risk Management tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Risk Management tab");

        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Label())) {
            error = "Failed to wait for Job Safety Analysis tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Label())) {
            error = "Failed to click Job Safety Analysis tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis tab");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(5000);
        
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
        
        //narrator.stepPassedWithScreenShot("Successfully navigated ");
        return true;
    }

    public boolean Job_Safety_Analysis() {
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Process_Flow_Button())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Process_Flow_Button())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Process flow' button.");

        //Business unit
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Business_unit_drop_down())) {
            error = "Failed to wait for Business unit drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Business_unit_drop_down())) {
            error = "Failed to click on Business unit drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Business unit 1")))) {
            error = "Failed to wait for Business unit drop down option :" + getData("Business unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition_option(getData("Business unit 1")))) {
            error = "Failed to click on Business unit drop down option :" + getData("Business unit 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition_option(getData("Business unit 2")))) {
            error = "Failed to click on Business unit drop down option :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Business unit 3")))) {
            error = "Failed to click on Business unit drop down option :" + getData("Business unit 3");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked drop down option: " + getData("Business unit 1") + " -> " + getData("Business unit 2") + " -> " + getData("Business unit 3"));

        if (getData("Project link").equalsIgnoreCase("Yes")){
            //Project link 
            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Project_link())) {
                error = "Failed to click on projetc link.";
                return false;
            }

//            //Project
//            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Project_Dropdown())) {
//                error = "Failed to wait for Project dropdown";
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Project_Dropdown())) {
//                error = "Failed to click on Project dropdown";
//                return false;
//            }
//             if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Project_Option(getData("Project")))) {
//                error = "Failed to wait for Project option: " + getData("Project");
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Project_Option(getData("Project")))) {
//                error = "Failed to click on Project option: " + getData("Project");
//                return false;
//            }
//            narrator.stepPassedWithScreenShot("Project option: " + getData("Project"));
//            
            //Related stakeholder
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Related_stakeholder_drop_down())) {
                error = "Failed to wait for Related stakeholder drop down";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Related_stakeholder_drop_down())) {
                error = "Failed to click on Related stakeholder drop down";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Related stakeholder")))) {
                error = "Failed to wait for option: " + getData("Related stakeholder");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Related stakeholder")))) {
                error = "Failed to click on option: " + getData("Related stakeholder");
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully slected related stakeholder: " + getData("Related stakeholder"));
        }
               
        //Process/Activity
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Process_Activity_drop_down())) {
            error = "Failed to wait for Process/Activity drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Process_Activity_drop_down())) {
            error = "Failed to click on Process/Activity drop down";
            return false;
        }        
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Process_option(getData("Process Activity 1")))) {
            error = "Failed to wait for option: " + getData("Process Activity 1");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Process_option(getData("Process Activity 1")))) {
            error = "Failed to click on Process Activity option: " + getData("Process Activity 1");
            return false;
        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Process Activity 2")))) {
//            error = "Failed to click on Process Activity option: " + getData("Process Activity 2");
//            return false;
//        }
        narrator.stepPassedWithScreenShot("Successfully slected Process Activity: " +  getData("Process Activity 1") + " -> " + getData("Process Activity 2"));

        //JSA title
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_title())) {
            error = "Failed to wait for JSA title";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_title(), getData("JSA title"))) {
            error = "Failed to enter JSA title :" + getData("JSA title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered JSA title :" + getData("JSA title"));

        //JSA scope description
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_scope_description())) {
            error = "Failed to wait for JSA scope description";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_scope_description(), getData("JSA scope description"))) {
            error = "Failed to enter JSA scope description :" + getData("JSA scope description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered JSA scope description:" + getData("JSA scope description"));

        //Relevant procedure
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Relevant_procedure_drop_down())) {
            error = "Failed to wait for Relevant procedure drop down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Relevant_procedure_drop_down())) {
            error = "Failed to click on Relevant procedure  drop down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Relevant procedure")))) {
            error = "Failed to wait for Relevant procedure option :" + getData("Relevant procedure");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Relevant procedure")))) {
            error = "Failed to click on Relevant procedure option :" + getData("Relevant procedure");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Relevant procedure option: " + getData("Relevant procedure"));
        
        //Assessment date
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.AssessmentDate())) {
            error = "Failed to wait for Assessment date Start date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.AssessmentDate(), startDate)) {
            error = "Failed to enter Assessment date '" + startDate + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Assessment date: '" + startDate + "'.");

        //Validity date
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.ValidityDate())) {
            error = "Failed to wait for Validity date Start date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.ValidityDate(), endDate)) {
            error = "Failed to enter Validity date '" + endDate + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully entered Validity date: '" + endDate + "'.");

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Responsible_person_drop_down())) {
            error = "Failed to wait for Responsible person drop down ";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Responsible_person_drop_down())) {
            error = "Failed to wait for Responsible person drop down ";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Responsible person")))) {
            error = "Failed to wait for Responsible person option :" + getData("Responsible person");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Responsible person")))) {
            error = "Failed to click on Responsible person option :" + getData("Responsible person");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Responsible person option : " + getData("Responsible person"));
        
        //Create JSA from
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Create_JSA_Drop_Down())) {
            error = "Failed to wait for Create JSA Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Create_JSA_Drop_Down())) {
            error = "Failed to click on Create JSA Drop Down";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Create JSA from")))) {
            error = "Failed to wait for Create JSA from option :" + getData("Create JSA from");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.visible_drop_down_transition(getData("Create JSA from")))) {
            error = "Failed to click on Create JSA from option :" + getData("Create JSA from");
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked Create JSA option : " + getData("Create JSA from"));

        if(getData("Save to continue").equalsIgnoreCase("Yes")){
            if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.saveToContinue_Button())){
                error = "Failed to wait for Save to continue button.";
                return false;
            }
            if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.saveToContinue_Button())){
                error = "Failed to wait for Save to continue button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save to continue button.");
        } else {
            //Save
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.saveBtn())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.saveBtn())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
            narrator.stepPassedWithScreenShot("Successfully clicked the save button.");
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.saveWait2(), 400)) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validation
        String saved = "";
        if (SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Record_Saved_popup())) {
            saved = SeleniumDriverInstance.retrieveTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.Record_Saved_popup());
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Record_Saved_popup())) {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        if (saved.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Successfully clicked save");
        } else {
            if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.failed())) {
                error = "Failed to wait for error message.";
                return false;
            }
            String failed = SeleniumDriverInstance.retrieveTextByXpath(Job_Safety_Analysis_V5_2_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved")) {
                error = "Failed to save record.";
                return false;
            }
        }
        
        pause(10000);
        narrator.stepPassedWithScreenShot(saved + ": successfully.");
        
//        //Job Safety Analysis Assessment
//        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Tab())) {
//            error = "Failed to wait for Job Safety Analysis Assessment tab.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Tab())) {
//            error = "Failed to click Job Safety Analysis Assessment tab.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Assessment tab");
//        
//        //Analysis Assessment Record
//        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Record())) {
//            error = "Failed to wait for Job Safety Analysis Assessment Record.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Record())) {
//            error = "Failed to click Job Safety Analysis Assessment Record";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Assessment Record");
//        
//        
//        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_ProcessFlow())){
//            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_ProcessFlow())){
//                error = "Failed to wait for Job Safety Analysis Assessment Process Flow.";
//                return false;
//            }
//        }
//        pause(2000);
//        
//        //Close Button
//        if(!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_CloseButton())){
//            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_CloseButton())){
//                error = "Failed to wait for the JSA Assessment close button.";
//                return false;
//            }
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.JSA_Assessment_CloseButton())){
//            error = "Failed to click the JSA Assessment close button.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked JSA Assessment.");
//        pause(3000);
//        //Job Safety Analysis Assessment
//        if (!SeleniumDriverInstance.waitForElementByXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Tab())) {
//            error = "Failed to wait for Job Safety Analysis Assessment tab.";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Job_Safety_Analysis_V5_2_PageObjects.Job_Safety_Analysis_Assessment_Tab())) {
//            error = "Failed to click Job Safety Analysis Assessment tab.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked Job Safety Analysis Assessment tab");
//        
        return true;
    }

}
