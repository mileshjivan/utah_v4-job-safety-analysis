/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SMabe
 */
public class S5_2_Job_Safety_Analysis_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S5_2_Job_Safety_Analysis_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    @Test
    public void S5_2_Job_Safety_Analysis_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR1-Record A Job Safety Analysis - Main Scenario
    @Test
    public void FR1_RecordAJobSafetyAnalysis_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis v5.2\\FR1-Record A Job Safety Analysis - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_RecordAJobSafetyAnalysis_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis v5.2\\FR1-Record A Job Safety Analysis - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_RecordAJobSafetyAnalysis_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis v5.2\\FR1-Record A Job Safety Analysis - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR2-Update Or Capture Job Safety Analysis Assessment - Main Scenario
    @Test
    public void FR2_UpdateOrCaptureJobSafetyAnalysisAssessment_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis v5.2\\FR2-Update Or Capture Job Safety Analysis Assessment - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR2_UpdateOrCaptureJobSafetyAnalysisAssessment_AlternateScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis v5.2\\FR2-Update Or Capture Job Safety Analysis Assessment - Alternate Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //UC JSA0202 - Add Job Safety Analysis Assessment - Main Scenario
    @Test
    public void UCJSA0202_Add_Job_Safety_Analysis_Assessment_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis v5.2\\UCJSA0202-Add job safety analysis assessment - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR3-View Related Incidents - Main Scenario
    @Test
    public void FR3_View_Related_Incidents_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis v5.2\\FR3-View Related Incidents - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR4-Capture JSA Actions - Main Scenario
    @Test
    public void FR4_Capture_JSA_Actions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Job Safety Analysis v5.2\\FR4-Capture JSA Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
